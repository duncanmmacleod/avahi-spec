# avahi

Avahi is a system which facilitates service discovery on a local network via the mDNS/DNS-SD protocol suite.

This repo contains the RPM spec file to enable creating binary RPMs for `avahi`.

## Updating this repo to the latest template

```
python -m cookiecutter https://git.ligo.org/packaging/rhel/spec-repo-template --overwrite-if-exists --output-dir .. --no-input
```
